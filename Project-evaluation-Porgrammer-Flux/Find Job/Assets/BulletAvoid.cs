using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;
using System.Linq;
public class BulletAvoid : MonoBehaviour{
    public List<GameObject> bulletsInside = new List<GameObject>();
    public Vector3 avoid;
    public float avoidFactor;
    public Enemy enemy;

    public LayerMask layer;

    private void Awake(){
        enemy = GetComponent<Enemy>();
    }
    private void Update(){
        if(!enemy.alwaysDodge) return;
        Debug.DrawRay(transform.position, avoid*100f, Color.yellow, 10f);
    }

    private void FixedUpdate(){
        CheckBulletsAhead();
    }
    
    private void LateUpdate(){
        avoid = Vector3.zero;
    }

    private void OnDrawGizmos(){
        //Gizmos.DrawSphere(transform.position + transform.up * 100f, 100f);
    }

    private void CheckBulletsAhead(){
        var inside = Physics2D.OverlapCircleAll(transform.position + transform.up * 100f, 100f, layer);
        foreach(var b in inside){
            if(CheckPath(b.gameObject, out var point)){
                print(point);
                var dir = transform.position - point;
                dir.Normalize();
                avoid += dir;
            }
        }

        
    }
    private bool CheckPath(GameObject bullet, out Vector3 point){
        point = Vector3.zero;
        if(!bullet.CompareTag("Shot")) return false;

        var rb = bullet.GetComponent<Rigidbody2D>();
        var hit = Physics2D.Raycast(bullet.transform.position, rb.velocity.normalized, rb.velocity.magnitude);

        if(hit.collider.gameObject == this.gameObject){
            point = hit.point;
            return true;
        }
        
        return false;
    }


}