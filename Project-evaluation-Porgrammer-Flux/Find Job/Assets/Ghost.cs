using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;

public struct PositionRotation{
    public Vector3 position;
    public Quaternion rotation;
    public PositionRotation(Vector3 position, Quaternion rotation){
        this.position = position;
        this.rotation = rotation;
    }
}
public class Ghost : MonoBehaviour{
    public List<PositionRotation> history = new List<PositionRotation>();
    public float delayTimeInSecs;
    
    private void Start(){
        StartCoroutine("Record");
        StartCoroutine("Move");
    }

    public IEnumerator Record(){
        while(true){
            if(ControlGame.paused || !Statics.Player) goto skip;
            history.Add(new PositionRotation(Statics.Player.transform.position, Statics.Player.transform.rotation));
            skip:
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator Move(){
        yield return new WaitForSeconds(delayTimeInSecs);
        while(true){
            if(ControlGame.paused || !Statics.Player) goto skip;
            transform.position = history[0].position;
            transform.rotation = history[0].rotation;
            history.RemoveAt(0);
            skip:
            yield return new WaitForEndOfFrame();
        }
    }
}