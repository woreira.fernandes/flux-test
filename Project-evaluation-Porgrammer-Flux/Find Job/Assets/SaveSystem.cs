using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem{

    public static string path = Application.persistentDataPath + "/fluxtestgame.savegame";
    public static void Save(){
        
        BinaryFormatter formatter = new BinaryFormatter();
        SaveFile sf = new SaveFile(Gaminho.Statics.CurrentLevel);
        FileStream fs = new FileStream(path, FileMode.Create);
        formatter.Serialize(fs, sf);
        fs.Close();
        Debug.Log("game saved");
    }

    public static void Load(){

        if(File.Exists(path)){

            BinaryFormatter formatter = new BinaryFormatter();
            FileStream fs = new FileStream(path, FileMode.Open);
            SaveFile sf = (SaveFile)formatter.Deserialize(fs);
            fs.Close();

           Gaminho.Statics.CurrentLevel = (int)sf.data[0];

           Debug.Log("game loaded");

        }else{

            Debug.Log("Save file not found in " + path); 
        }
    }
    

    public static bool CheckProgress(){
        return File.Exists(path);
    }
}

[System.Serializable]
public class SaveFile{
    public object[] data;

    public SaveFile(params object[] data){
        this.data = data;
    }
}