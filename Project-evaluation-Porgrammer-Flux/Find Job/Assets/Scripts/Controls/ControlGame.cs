﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife, BarShield;

    public Text pausedText;

    public static bool paused;
    
    // Use this for initialization
    void Start () {
        Statics.EnemiesDead = 0;
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        pausedText.gameObject.SetActive(paused);
        
    }

    private void Update()
    {

        if(Input.GetKeyDown("p")){
            if(paused) Unpause();
            else Pause();
        }

        if(paused) return;
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);
        if (!Statics.Player) return;
        BarShield.parent.gameObject.SetActive(Statics.WithShield);

        BarShield.localScale = new Vector3(ControlShip.lifeShield / 10f, 1, 1);
        BarShield.parent.position = Statics.Player.transform.position + Vector3.up * 100f;
    }

    public void LevelPassed()
    {
        
        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;
        if (Statics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }

        SaveSystem.Save();
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    public void Pause(){
        if(paused) return;
        paused = true;
        Time.timeScale = 0f;
        var allSources = FindObjectsOfType<AudioSource>();
        foreach(var s in allSources){
            s.Pause();
        }

        pausedText.gameObject.SetActive(paused);
    }

    public void Unpause(){
        if(!paused) return;
        paused = false;
        Time.timeScale = 1f;
        var allSources = FindObjectsOfType<AudioSource>();
        foreach(var s in allSources){
            s.UnPause();
        }

        pausedText.gameObject.SetActive(paused);
    }
}
