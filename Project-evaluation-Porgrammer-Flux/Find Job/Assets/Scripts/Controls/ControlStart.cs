﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlStart : MonoBehaviour {
    public Text Record;
    public Button buttonContinue;
	// Use this for initialization
	void Start () {

        buttonContinue.interactable = SaveSystem.CheckProgress();
        
        Statics.WithShield = false;
        Statics.EnemiesDead = 0;
        Statics.CurrentLevel = 0;
        Statics.Points = 0;
        Statics.ShootingSelected = 2;
        //Loads Record
        if (PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) == 0)
        {
            PlayerPrefs.SetString(Statics.PLAYERPREF_NEWRECORD, "Nobody");
        }
        Record.text = "Record: " + PlayerPrefs.GetString(Statics.PLAYERPREF_NEWRECORD) + "(" + PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) + ")";
       
	}

	public void StartClick()
    {
        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    public void ContinueClick()
    {
        SaveSystem.Load();
        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    public void Quit()
    {
        Application.Quit();
    }

    
}
