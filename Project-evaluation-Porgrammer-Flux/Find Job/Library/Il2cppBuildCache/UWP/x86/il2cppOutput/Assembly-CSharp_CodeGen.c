﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void BulletAvoid::Awake()
extern void BulletAvoid_Awake_mEECC42A3DA8906CF14547D3DC2BC21C4EC8253C3 (void);
// 0x00000002 System.Void BulletAvoid::Update()
extern void BulletAvoid_Update_m6385FA441126DDC5322802E81716EE4077EE5BE0 (void);
// 0x00000003 System.Void BulletAvoid::FixedUpdate()
extern void BulletAvoid_FixedUpdate_m8074D48E146E9ACE9F50ADCFF5FF1AE8D7FE5585 (void);
// 0x00000004 System.Void BulletAvoid::LateUpdate()
extern void BulletAvoid_LateUpdate_m19161D5A3612D004BB2BC2ABA5CA0AE18EDB4F67 (void);
// 0x00000005 System.Void BulletAvoid::OnDrawGizmos()
extern void BulletAvoid_OnDrawGizmos_m5D8885D614864B67823E6A2C5D8F23EF602B7565 (void);
// 0x00000006 System.Void BulletAvoid::CheckBulletsAhead()
extern void BulletAvoid_CheckBulletsAhead_mC0080683FCA8C0C74C40432A450DE454433B7988 (void);
// 0x00000007 System.Boolean BulletAvoid::CheckPath(UnityEngine.GameObject,UnityEngine.Vector3&)
extern void BulletAvoid_CheckPath_mEA4C34B3D6E70A6ECE7AA38997AB04300500E94E (void);
// 0x00000008 System.Void BulletAvoid::.ctor()
extern void BulletAvoid__ctor_m8BEC5D256760E11FBF5DEEB182EB370D498EC074 (void);
// 0x00000009 System.Void PositionRotation::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void PositionRotation__ctor_m8001FB5D904D433CCE64B26A1A078370CF939CEA (void);
// 0x0000000A System.Void Ghost::Start()
extern void Ghost_Start_m4D58708DF0B0990AD546DD02C95396D5C3BE6D11 (void);
// 0x0000000B System.Collections.IEnumerator Ghost::Record()
extern void Ghost_Record_m4168013832A89373C1747BDAB944AA4D386EFAFC (void);
// 0x0000000C System.Collections.IEnumerator Ghost::Move()
extern void Ghost_Move_mAFF4236BBE1B20C0AC72FF15073CA0767AD0B610 (void);
// 0x0000000D System.Void Ghost::.ctor()
extern void Ghost__ctor_mD85306F4823FAF1C651A2FEBE432C090D2F34948 (void);
// 0x0000000E System.Void Ghost/<Record>d__3::.ctor(System.Int32)
extern void U3CRecordU3Ed__3__ctor_mDADC7D06B19D1FA156883AAD16CD24FB2659679D (void);
// 0x0000000F System.Void Ghost/<Record>d__3::System.IDisposable.Dispose()
extern void U3CRecordU3Ed__3_System_IDisposable_Dispose_m3FB4388DCAF595FDE68B171DD4C7FE9333649631 (void);
// 0x00000010 System.Boolean Ghost/<Record>d__3::MoveNext()
extern void U3CRecordU3Ed__3_MoveNext_m7B74E697989C45C6117D08C4516F48DAC9D903FC (void);
// 0x00000011 System.Object Ghost/<Record>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRecordU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09F62161F2D2EC6FA1697EC4070BAF95056D9FCE (void);
// 0x00000012 System.Void Ghost/<Record>d__3::System.Collections.IEnumerator.Reset()
extern void U3CRecordU3Ed__3_System_Collections_IEnumerator_Reset_m48CEE49AA2C6B0A02F85C5F610619A6A0329B854 (void);
// 0x00000013 System.Object Ghost/<Record>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CRecordU3Ed__3_System_Collections_IEnumerator_get_Current_m348C3E19C90062DEA90DAAE5DBB1BD7C164805F4 (void);
// 0x00000014 System.Void Ghost/<Move>d__4::.ctor(System.Int32)
extern void U3CMoveU3Ed__4__ctor_m39378E493B59AF7A53F857330885AEB62BF8567D (void);
// 0x00000015 System.Void Ghost/<Move>d__4::System.IDisposable.Dispose()
extern void U3CMoveU3Ed__4_System_IDisposable_Dispose_mA26B8D351B36B7BB3A525BA91D636757C0477A0A (void);
// 0x00000016 System.Boolean Ghost/<Move>d__4::MoveNext()
extern void U3CMoveU3Ed__4_MoveNext_mB48E8EE9A2366B5FF01FEC0A6792AE98A2C3AD86 (void);
// 0x00000017 System.Object Ghost/<Move>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD87C40DE9C6032BBCE553AF6C0B8BB382FB8448E (void);
// 0x00000018 System.Void Ghost/<Move>d__4::System.Collections.IEnumerator.Reset()
extern void U3CMoveU3Ed__4_System_Collections_IEnumerator_Reset_mDD0C19AD40F878D74DE1A348B2D01D394622BCA8 (void);
// 0x00000019 System.Object Ghost/<Move>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CMoveU3Ed__4_System_Collections_IEnumerator_get_Current_m70904F2D3D7BC472D2B9D84FCCE661BDF35D3268 (void);
// 0x0000001A System.Void SaveSystem::Save()
extern void SaveSystem_Save_m851BAD9974D7FC3C358D9E01D868F4F9B4611C35 (void);
// 0x0000001B System.Void SaveSystem::Load()
extern void SaveSystem_Load_mCD05A8AB4AD238ADCB98836DC7B0EEDB0AD3BA41 (void);
// 0x0000001C System.Boolean SaveSystem::CheckProgress()
extern void SaveSystem_CheckProgress_m7AE62118E4471BEE030A184FB8A78770D7BC9C05 (void);
// 0x0000001D System.Void SaveSystem::.cctor()
extern void SaveSystem__cctor_m42C732C921FB437710BBF9A8F3D42981A6906ADA (void);
// 0x0000001E System.Void SaveFile::.ctor(System.Object[])
extern void SaveFile__ctor_mACFE7F723FAB00271A34D181679F5761C2BCF707 (void);
// 0x0000001F System.Void Life::TakesLife(System.Int32)
extern void Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD (void);
// 0x00000020 System.Void Life::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178 (void);
// 0x00000021 System.Void Life::.ctor()
extern void Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926 (void);
// 0x00000022 System.Void ControlGame::Start()
extern void ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225 (void);
// 0x00000023 System.Void ControlGame::Update()
extern void ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50 (void);
// 0x00000024 System.Void ControlGame::LevelPassed()
extern void ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534 (void);
// 0x00000025 System.Void ControlGame::GameOver()
extern void ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13 (void);
// 0x00000026 System.Void ControlGame::Clear()
extern void ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074 (void);
// 0x00000027 System.Void ControlGame::Pause()
extern void ControlGame_Pause_mAC008AF2227E9842D1734F64AC10416999D1469E (void);
// 0x00000028 System.Void ControlGame::Unpause()
extern void ControlGame_Unpause_m22422238578CECAE6E45EE4F754B9FD49B66F073 (void);
// 0x00000029 System.Void ControlGame::.ctor()
extern void ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D (void);
// 0x0000002A System.Void ControlStart::Start()
extern void ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51 (void);
// 0x0000002B System.Void ControlStart::StartClick()
extern void ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB (void);
// 0x0000002C System.Void ControlStart::ContinueClick()
extern void ControlStart_ContinueClick_mBD000E184A0B18DFBA59C7536536E203CD989867 (void);
// 0x0000002D System.Void ControlStart::Quit()
extern void ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747 (void);
// 0x0000002E System.Void ControlStart::.ctor()
extern void ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA (void);
// 0x0000002F System.Void EnemyControl::Start()
extern void EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F (void);
// 0x00000030 System.Collections.IEnumerator EnemyControl::Process()
extern void EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B (void);
// 0x00000031 System.Void EnemyControl::EnemyCreate()
extern void EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E (void);
// 0x00000032 System.Collections.IEnumerator EnemyControl::CallBoss()
extern void EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101 (void);
// 0x00000033 System.Void EnemyControl::.ctor()
extern void EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF (void);
// 0x00000034 System.Void EnemyControl/<Process>d__4::.ctor(System.Int32)
extern void U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15 (void);
// 0x00000035 System.Void EnemyControl/<Process>d__4::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2 (void);
// 0x00000036 System.Boolean EnemyControl/<Process>d__4::MoveNext()
extern void U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC (void);
// 0x00000037 System.Object EnemyControl/<Process>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B (void);
// 0x00000038 System.Void EnemyControl/<Process>d__4::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D (void);
// 0x00000039 System.Object EnemyControl/<Process>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996 (void);
// 0x0000003A System.Void EnemyControl/<CallBoss>d__6::.ctor(System.Int32)
extern void U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363 (void);
// 0x0000003B System.Void EnemyControl/<CallBoss>d__6::System.IDisposable.Dispose()
extern void U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9 (void);
// 0x0000003C System.Boolean EnemyControl/<CallBoss>d__6::MoveNext()
extern void U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909 (void);
// 0x0000003D System.Object EnemyControl/<CallBoss>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9 (void);
// 0x0000003E System.Void EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D (void);
// 0x0000003F System.Object EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27 (void);
// 0x00000040 System.Void RecordControl::Start()
extern void RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F (void);
// 0x00000041 System.Void RecordControl::UpdateName()
extern void RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC (void);
// 0x00000042 System.Void RecordControl::.ctor()
extern void RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4 (void);
// 0x00000043 System.Void Boss2::Start()
extern void Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8 (void);
// 0x00000044 System.Collections.IEnumerator Boss2::ShowParts()
extern void Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315 (void);
// 0x00000045 System.Void Boss2::Update()
extern void Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14 (void);
// 0x00000046 System.Collections.IEnumerator Boss2::Shot()
extern void Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6 (void);
// 0x00000047 UnityEngine.GameObject Boss2::GetOneActive()
extern void Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3 (void);
// 0x00000048 System.Void Boss2::KillMe(UnityEngine.GameObject)
extern void Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285 (void);
// 0x00000049 System.Void Boss2::.ctor()
extern void Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F (void);
// 0x0000004A System.Void Boss2/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8 (void);
// 0x0000004B System.Void Boss2/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146 (void);
// 0x0000004C System.Boolean Boss2/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704 (void);
// 0x0000004D System.Object Boss2/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41 (void);
// 0x0000004E System.Void Boss2/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D (void);
// 0x0000004F System.Object Boss2/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A (void);
// 0x00000050 System.Void Boss2/<Shot>d__7::.ctor(System.Int32)
extern void U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C (void);
// 0x00000051 System.Void Boss2/<Shot>d__7::System.IDisposable.Dispose()
extern void U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7 (void);
// 0x00000052 System.Boolean Boss2/<Shot>d__7::MoveNext()
extern void U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE (void);
// 0x00000053 System.Object Boss2/<Shot>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6 (void);
// 0x00000054 System.Void Boss2/<Shot>d__7::System.Collections.IEnumerator.Reset()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62 (void);
// 0x00000055 System.Object Boss2/<Shot>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25 (void);
// 0x00000056 System.Void Boss3::Start()
extern void Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C (void);
// 0x00000057 System.Collections.IEnumerator Boss3::ShowParts(System.Boolean)
extern void Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6 (void);
// 0x00000058 System.Collections.IEnumerator Boss3::AttackNow()
extern void Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420 (void);
// 0x00000059 System.Collections.IEnumerator Boss3::Attack(UnityEngine.UI.Image)
extern void Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006 (void);
// 0x0000005A UnityEngine.GameObject Boss3::GetOneActive()
extern void Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F (void);
// 0x0000005B System.Void Boss3::KillMe(UnityEngine.GameObject)
extern void Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029 (void);
// 0x0000005C System.Void Boss3::.ctor()
extern void Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306 (void);
// 0x0000005D System.Void Boss3/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D (void);
// 0x0000005E System.Void Boss3/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962 (void);
// 0x0000005F System.Boolean Boss3/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77 (void);
// 0x00000060 System.Object Boss3/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F (void);
// 0x00000061 System.Void Boss3/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524 (void);
// 0x00000062 System.Object Boss3/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF (void);
// 0x00000063 System.Void Boss3/<AttackNow>d__6::.ctor(System.Int32)
extern void U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9 (void);
// 0x00000064 System.Void Boss3/<AttackNow>d__6::System.IDisposable.Dispose()
extern void U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521 (void);
// 0x00000065 System.Boolean Boss3/<AttackNow>d__6::MoveNext()
extern void U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7 (void);
// 0x00000066 System.Object Boss3/<AttackNow>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA (void);
// 0x00000067 System.Void Boss3/<AttackNow>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A (void);
// 0x00000068 System.Object Boss3/<AttackNow>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0 (void);
// 0x00000069 System.Void Boss3/<Attack>d__7::.ctor(System.Int32)
extern void U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327 (void);
// 0x0000006A System.Void Boss3/<Attack>d__7::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF (void);
// 0x0000006B System.Boolean Boss3/<Attack>d__7::MoveNext()
extern void U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301 (void);
// 0x0000006C System.Object Boss3/<Attack>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52 (void);
// 0x0000006D System.Void Boss3/<Attack>d__7::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7 (void);
// 0x0000006E System.Object Boss3/<Attack>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F (void);
// 0x0000006F System.Void Enemy::Awake()
extern void Enemy_Awake_mF268033197059561A4A0BC3E5F6B83B50D29C861 (void);
// 0x00000070 System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x00000071 System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x00000072 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000073 System.Void Enemy::MyDeath()
extern void Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8 (void);
// 0x00000074 System.Collections.IEnumerator Enemy::KillMe()
extern void Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5 (void);
// 0x00000075 System.Void Enemy::Create(System.Int32)
extern void Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759 (void);
// 0x00000076 System.Collections.IEnumerator Enemy::Shoot()
extern void Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767 (void);
// 0x00000077 System.Void Enemy::EndBoss()
extern void Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA (void);
// 0x00000078 System.Void Enemy::PStick()
extern void Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173 (void);
// 0x00000079 System.Void Enemy::Avoid()
extern void Enemy_Avoid_m59A62B1CAA4D1F208BC5B9C4A71880262CC8DF6D (void);
// 0x0000007A System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x0000007B System.Void Enemy/<KillMe>d__13::.ctor(System.Int32)
extern void U3CKillMeU3Ed__13__ctor_m8C885CDD9C15822AFC09DE23F1D2E62CED4B313E (void);
// 0x0000007C System.Void Enemy/<KillMe>d__13::System.IDisposable.Dispose()
extern void U3CKillMeU3Ed__13_System_IDisposable_Dispose_m0CD4019C67BDACBFC48918B184B1DFF9C8D9F3C6 (void);
// 0x0000007D System.Boolean Enemy/<KillMe>d__13::MoveNext()
extern void U3CKillMeU3Ed__13_MoveNext_m1A1DDE29A0C75F5DBAB55884EFA30FA79502A749 (void);
// 0x0000007E System.Object Enemy/<KillMe>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillMeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE6C9A72BE699E31F04969B56C20CDD36E626BBF (void);
// 0x0000007F System.Void Enemy/<KillMe>d__13::System.Collections.IEnumerator.Reset()
extern void U3CKillMeU3Ed__13_System_Collections_IEnumerator_Reset_m282DE72907854BC7299C5B6F867AA8902B3C4FFF (void);
// 0x00000080 System.Object Enemy/<KillMe>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CKillMeU3Ed__13_System_Collections_IEnumerator_get_Current_mF5BA2677AAF2A8BEE42D759437BB8302D36B8B2C (void);
// 0x00000081 System.Void Enemy/<Shoot>d__15::.ctor(System.Int32)
extern void U3CShootU3Ed__15__ctor_m29B2F0E858838B3DB93DED23561CABBF841230F6 (void);
// 0x00000082 System.Void Enemy/<Shoot>d__15::System.IDisposable.Dispose()
extern void U3CShootU3Ed__15_System_IDisposable_Dispose_m14420DD502FAEEB08651392992627F1D36774088 (void);
// 0x00000083 System.Boolean Enemy/<Shoot>d__15::MoveNext()
extern void U3CShootU3Ed__15_MoveNext_m8A389B245E6892D75DD4F4E6F05FCE10F2F2FA65 (void);
// 0x00000084 System.Object Enemy/<Shoot>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E0354BC40D11DF17FE0263C2033AE2028A1C752 (void);
// 0x00000085 System.Void Enemy/<Shoot>d__15::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__15_System_Collections_IEnumerator_Reset_mF264D9F35C68E5C483C9FEE60B2504A8D00F7158 (void);
// 0x00000086 System.Object Enemy/<Shoot>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__15_System_Collections_IEnumerator_get_Current_mDE949D6761D3AA103FD853EB1A04D36B45173FCB (void);
// 0x00000087 System.Void ItemDrop::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7 (void);
// 0x00000088 System.Void ItemDrop::.ctor()
extern void ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2 (void);
// 0x00000089 System.Void CallScene::Call(System.String)
extern void CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE (void);
// 0x0000008A System.Void CallScene::.ctor()
extern void CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C (void);
// 0x0000008B System.Void DestroyTime::Start()
extern void DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497 (void);
// 0x0000008C System.Collections.IEnumerator DestroyTime::CallDestroy()
extern void DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6 (void);
// 0x0000008D System.Void DestroyTime::.ctor()
extern void DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88 (void);
// 0x0000008E System.Void DestroyTime/<CallDestroy>d__2::.ctor(System.Int32)
extern void U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF (void);
// 0x0000008F System.Void DestroyTime/<CallDestroy>d__2::System.IDisposable.Dispose()
extern void U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD (void);
// 0x00000090 System.Boolean DestroyTime/<CallDestroy>d__2::MoveNext()
extern void U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B (void);
// 0x00000091 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2 (void);
// 0x00000092 System.Void DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9 (void);
// 0x00000093 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0 (void);
// 0x00000094 System.Void Explosion::Start()
extern void Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22 (void);
// 0x00000095 System.Collections.IEnumerator Explosion::Explode()
extern void Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258 (void);
// 0x00000096 System.Void Explosion::.ctor()
extern void Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094 (void);
// 0x00000097 System.Void Explosion/<Explode>d__3::.ctor(System.Int32)
extern void U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F (void);
// 0x00000098 System.Void Explosion/<Explode>d__3::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8 (void);
// 0x00000099 System.Boolean Explosion/<Explode>d__3::MoveNext()
extern void U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC (void);
// 0x0000009A System.Object Explosion/<Explode>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02 (void);
// 0x0000009B System.Void Explosion/<Explode>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7 (void);
// 0x0000009C System.Object Explosion/<Explode>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB (void);
// 0x0000009D Stick/stck Stick::GetStck()
extern void Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B (void);
// 0x0000009E System.Void ControlShip::Start()
extern void ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8 (void);
// 0x0000009F System.Void ControlShip::FixedUpdate()
extern void ControlShip_FixedUpdate_m3C8FE438C2E4B684A855C39792BAA7678C2A4ECF (void);
// 0x000000A0 System.Void ControlShip::LateUpdate()
extern void ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D (void);
// 0x000000A1 System.Void ControlShip::AnimateMotor()
extern void ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27 (void);
// 0x000000A2 System.Collections.IEnumerator ControlShip::Shoot()
extern void ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA (void);
// 0x000000A3 System.Void ControlShip::CallShield()
extern void ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C (void);
// 0x000000A4 System.Void ControlShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73 (void);
// 0x000000A5 System.Void ControlShip::.ctor()
extern void ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93 (void);
// 0x000000A6 System.Void ControlShip/<Shoot>d__16::.ctor(System.Int32)
extern void U3CShootU3Ed__16__ctor_m34916B429E472DCCAB8545976B05E21A0F82EDD1 (void);
// 0x000000A7 System.Void ControlShip/<Shoot>d__16::System.IDisposable.Dispose()
extern void U3CShootU3Ed__16_System_IDisposable_Dispose_mDEEB2EF12BA67674E13E07F9B1D0A2FCA2F23FE5 (void);
// 0x000000A8 System.Boolean ControlShip/<Shoot>d__16::MoveNext()
extern void U3CShootU3Ed__16_MoveNext_mD63FDF8822EEB1B3960FBB0C45E03A3692DC0486 (void);
// 0x000000A9 System.Object ControlShip/<Shoot>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05E497F0401ABEAA6DC2D4014177198C08363B8F (void);
// 0x000000AA System.Void ControlShip/<Shoot>d__16::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__16_System_Collections_IEnumerator_Reset_m2945F9E62AACBFC5E77244CA23A9197100CFAAA0 (void);
// 0x000000AB System.Object ControlShip/<Shoot>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__16_System_Collections_IEnumerator_get_Current_mD9960268E8871B02290A54F2F1F7D1B12963E76D (void);
// 0x000000AC System.Void Flux.ReadMe::.ctor()
extern void ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56 (void);
// 0x000000AD System.Void Gaminho.Level::.ctor()
extern void Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9 (void);
// 0x000000AE System.Void Gaminho.ScenarioLimits::.ctor()
extern void ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B (void);
// 0x000000AF System.Void Gaminho.Shot::.ctor()
extern void Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3 (void);
// 0x000000B0 UnityEngine.Quaternion Gaminho.Statics::FaceObject(UnityEngine.Vector2,UnityEngine.Vector2,Gaminho.Statics/FacingDirection)
extern void Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50 (void);
// 0x000000B1 System.Void Gaminho.Statics::.cctor()
extern void Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4 (void);
static Il2CppMethodPointer s_methodPointers[177] = 
{
	BulletAvoid_Awake_mEECC42A3DA8906CF14547D3DC2BC21C4EC8253C3,
	BulletAvoid_Update_m6385FA441126DDC5322802E81716EE4077EE5BE0,
	BulletAvoid_FixedUpdate_m8074D48E146E9ACE9F50ADCFF5FF1AE8D7FE5585,
	BulletAvoid_LateUpdate_m19161D5A3612D004BB2BC2ABA5CA0AE18EDB4F67,
	BulletAvoid_OnDrawGizmos_m5D8885D614864B67823E6A2C5D8F23EF602B7565,
	BulletAvoid_CheckBulletsAhead_mC0080683FCA8C0C74C40432A450DE454433B7988,
	BulletAvoid_CheckPath_mEA4C34B3D6E70A6ECE7AA38997AB04300500E94E,
	BulletAvoid__ctor_m8BEC5D256760E11FBF5DEEB182EB370D498EC074,
	PositionRotation__ctor_m8001FB5D904D433CCE64B26A1A078370CF939CEA,
	Ghost_Start_m4D58708DF0B0990AD546DD02C95396D5C3BE6D11,
	Ghost_Record_m4168013832A89373C1747BDAB944AA4D386EFAFC,
	Ghost_Move_mAFF4236BBE1B20C0AC72FF15073CA0767AD0B610,
	Ghost__ctor_mD85306F4823FAF1C651A2FEBE432C090D2F34948,
	U3CRecordU3Ed__3__ctor_mDADC7D06B19D1FA156883AAD16CD24FB2659679D,
	U3CRecordU3Ed__3_System_IDisposable_Dispose_m3FB4388DCAF595FDE68B171DD4C7FE9333649631,
	U3CRecordU3Ed__3_MoveNext_m7B74E697989C45C6117D08C4516F48DAC9D903FC,
	U3CRecordU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09F62161F2D2EC6FA1697EC4070BAF95056D9FCE,
	U3CRecordU3Ed__3_System_Collections_IEnumerator_Reset_m48CEE49AA2C6B0A02F85C5F610619A6A0329B854,
	U3CRecordU3Ed__3_System_Collections_IEnumerator_get_Current_m348C3E19C90062DEA90DAAE5DBB1BD7C164805F4,
	U3CMoveU3Ed__4__ctor_m39378E493B59AF7A53F857330885AEB62BF8567D,
	U3CMoveU3Ed__4_System_IDisposable_Dispose_mA26B8D351B36B7BB3A525BA91D636757C0477A0A,
	U3CMoveU3Ed__4_MoveNext_mB48E8EE9A2366B5FF01FEC0A6792AE98A2C3AD86,
	U3CMoveU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD87C40DE9C6032BBCE553AF6C0B8BB382FB8448E,
	U3CMoveU3Ed__4_System_Collections_IEnumerator_Reset_mDD0C19AD40F878D74DE1A348B2D01D394622BCA8,
	U3CMoveU3Ed__4_System_Collections_IEnumerator_get_Current_m70904F2D3D7BC472D2B9D84FCCE661BDF35D3268,
	SaveSystem_Save_m851BAD9974D7FC3C358D9E01D868F4F9B4611C35,
	SaveSystem_Load_mCD05A8AB4AD238ADCB98836DC7B0EEDB0AD3BA41,
	SaveSystem_CheckProgress_m7AE62118E4471BEE030A184FB8A78770D7BC9C05,
	SaveSystem__cctor_m42C732C921FB437710BBF9A8F3D42981A6906ADA,
	SaveFile__ctor_mACFE7F723FAB00271A34D181679F5761C2BCF707,
	Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD,
	Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178,
	Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926,
	ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225,
	ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50,
	ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534,
	ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13,
	ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074,
	ControlGame_Pause_mAC008AF2227E9842D1734F64AC10416999D1469E,
	ControlGame_Unpause_m22422238578CECAE6E45EE4F754B9FD49B66F073,
	ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D,
	ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51,
	ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB,
	ControlStart_ContinueClick_mBD000E184A0B18DFBA59C7536536E203CD989867,
	ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747,
	ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA,
	EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F,
	EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E,
	EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF,
	U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15,
	U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2,
	U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC,
	U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996,
	U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363,
	U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9,
	U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909,
	U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27,
	RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F,
	RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC,
	RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4,
	Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8,
	Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14,
	Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3,
	Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285,
	Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F,
	U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE,
	U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C,
	Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F,
	Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029,
	Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306,
	U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962,
	U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF,
	U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9,
	U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521,
	U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7,
	U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0,
	U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327,
	U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF,
	U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301,
	U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F,
	Enemy_Awake_mF268033197059561A4A0BC3E5F6B83B50D29C861,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8,
	Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759,
	Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA,
	Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173,
	Enemy_Avoid_m59A62B1CAA4D1F208BC5B9C4A71880262CC8DF6D,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CKillMeU3Ed__13__ctor_m8C885CDD9C15822AFC09DE23F1D2E62CED4B313E,
	U3CKillMeU3Ed__13_System_IDisposable_Dispose_m0CD4019C67BDACBFC48918B184B1DFF9C8D9F3C6,
	U3CKillMeU3Ed__13_MoveNext_m1A1DDE29A0C75F5DBAB55884EFA30FA79502A749,
	U3CKillMeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE6C9A72BE699E31F04969B56C20CDD36E626BBF,
	U3CKillMeU3Ed__13_System_Collections_IEnumerator_Reset_m282DE72907854BC7299C5B6F867AA8902B3C4FFF,
	U3CKillMeU3Ed__13_System_Collections_IEnumerator_get_Current_mF5BA2677AAF2A8BEE42D759437BB8302D36B8B2C,
	U3CShootU3Ed__15__ctor_m29B2F0E858838B3DB93DED23561CABBF841230F6,
	U3CShootU3Ed__15_System_IDisposable_Dispose_m14420DD502FAEEB08651392992627F1D36774088,
	U3CShootU3Ed__15_MoveNext_m8A389B245E6892D75DD4F4E6F05FCE10F2F2FA65,
	U3CShootU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4E0354BC40D11DF17FE0263C2033AE2028A1C752,
	U3CShootU3Ed__15_System_Collections_IEnumerator_Reset_mF264D9F35C68E5C483C9FEE60B2504A8D00F7158,
	U3CShootU3Ed__15_System_Collections_IEnumerator_get_Current_mDE949D6761D3AA103FD853EB1A04D36B45173FCB,
	ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7,
	ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2,
	CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE,
	CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C,
	DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497,
	DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88,
	U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B,
	U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22,
	Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094,
	U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC,
	U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B,
	ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8,
	ControlShip_FixedUpdate_m3C8FE438C2E4B684A855C39792BAA7678C2A4ECF,
	ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D,
	ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27,
	ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C,
	ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73,
	ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93,
	U3CShootU3Ed__16__ctor_m34916B429E472DCCAB8545976B05E21A0F82EDD1,
	U3CShootU3Ed__16_System_IDisposable_Dispose_mDEEB2EF12BA67674E13E07F9B1D0A2FCA2F23FE5,
	U3CShootU3Ed__16_MoveNext_mD63FDF8822EEB1B3960FBB0C45E03A3692DC0486,
	U3CShootU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05E497F0401ABEAA6DC2D4014177198C08363B8F,
	U3CShootU3Ed__16_System_Collections_IEnumerator_Reset_m2945F9E62AACBFC5E77244CA23A9197100CFAAA0,
	U3CShootU3Ed__16_System_Collections_IEnumerator_get_Current_mD9960268E8871B02290A54F2F1F7D1B12963E76D,
	ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56,
	Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9,
	ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B,
	Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3,
	Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50,
	Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4,
};
extern void PositionRotation__ctor_m8001FB5D904D433CCE64B26A1A078370CF939CEA_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000009, PositionRotation__ctor_m8001FB5D904D433CCE64B26A1A078370CF939CEA_AdjustorThunk },
};
static const int32_t s_InvokerIndices[177] = 
{
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	654,
	1459,
	828,
	1459,
	1426,
	1426,
	1459,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	2386,
	2386,
	2379,
	2386,
	1251,
	1240,
	1251,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1459,
	1426,
	1459,
	1426,
	1459,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1459,
	1459,
	1459,
	1459,
	1426,
	1459,
	1426,
	1426,
	1251,
	1459,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1459,
	1014,
	1426,
	1013,
	1426,
	1251,
	1459,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1459,
	1459,
	1459,
	1251,
	1459,
	1426,
	1240,
	1426,
	1459,
	1459,
	1459,
	1459,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1251,
	1459,
	1251,
	1459,
	1459,
	1426,
	1459,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1459,
	1426,
	1459,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	2365,
	1459,
	1459,
	1459,
	1459,
	1426,
	1459,
	1251,
	1459,
	1240,
	1459,
	1446,
	1426,
	1459,
	1426,
	1459,
	1459,
	1459,
	1459,
	1895,
	2386,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	177,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
