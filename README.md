# Flux Programmer Evaluation Test  
### Project Files:   
**Github:** https://github.com/Woreira/flux-test2  
**GitLab:** https://gitlab.com/woreira.fernandes/flux-test  
*(make sure your git LFS is configured, or download the zip file)*  
**Google Drive:** https://drive.google.com/file/d/1HAf82GgOkJ_WzB_FxnZts6_fbPxWt3qy/view?usp=sharing  
  
*~Lucas Moreira*
